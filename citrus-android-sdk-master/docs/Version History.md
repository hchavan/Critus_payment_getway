| Version History<br>Last updated : 21/12/2015 |  Feature Updates|
| :-------------: | :------------- |
| v3.1.1| * Removed ic_launcher images from the SDK.  |
| v3.1.0| * New Link User Extended API  
| | * New Pay using Citrus Cash|
| | * Auto OTP feature enhancements|
| | * Updated Analytics Version|
| | * Bug fixes and improvements|
| v3.0.36| * Auto OTP feature enhancements  
| | * Bug fixes and improvements|
| v3.0.35| * Bug fixes and improvements  |
|  v3.0.34| * Added resized Card Type icons for all resolutions  
| | * Bug fixes and improvements|
| v3.0.33| * Auto OTP Reading on 3D Secure Pages |
|	| * Improved Speed |
|	| * Bug fixes and improvements |
| v3.0.3| * Added Additional check for card validation |
|	| * Removing unnecessary permission |
|	| * Added password field in case of Pay Using Citrus Cash |
|	| * Added convenience method to get the cardScheme in CardNumberEditText widget |
| v3.0.2| * BindByMobileNo API integrated |
|       | * Fix for SSL error |
|	| * Avoided multiple initialization of the CitrusClient |
|	| * Bug fixes |
